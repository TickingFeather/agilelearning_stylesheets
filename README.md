# AgileLearning stylesheets
🇬🇧  This is a basic repository for CSS stylesheets to customize the look of the AgileLearning platform, used in the OOP course at [Polytech Grenoble](https://polytech-grenoble.fr/).

All you have to do is copy the `user-stylesheet.css` file to `AgileLearning/workshops/cursus/contents/`, and in that same directory, edit the `outline.html` file to add
```html
<link rel="stylesheet" href="/user-stylesheet.css" type="text/css"/>
```
in the `<head>` tag (~ line 10 or so). By editing the CSS file, you can pick one of the predefined themes or define your own, just read the comments and have fun.

----

🇫🇷  Ceci est un dépôt basique pour des fichiers CSS pour personnaliser l'apparence de la plateforme AgileLearning, utilisée dans le cours de POO (Programmation Orientée Objet) à [Polytech Grenoble](https://polytech-grenoble.fr/).

Il vous suffit de copier le fichier `user-stylesheet.css` dans `AgileLearning/workshops/cursus/contents/`, et dans ce même répertoire, modifier le fichier `outline.html` pour ajouter
```html
<link rel="stylesheet" href="/user-stylesheet.css" type="text/css"/>
```
dans le `<head>` (~ ligne 10 environ). En modifiant le fichier CSS, vous pouvez choisir l'un des thèmes prédéfinis ou définir le vôtre, lisez les commentaires et amusez-vous.

